import React from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Thể thao",
    subtitle: "Thông tin về thể thao",
    imageurl: require("./images/sport.jpg"),
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Đời sống",
    subtitle: "Thông tin về đời sống",
    imageurl: require("./images/sport.jpg"),
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Thời sự",
    subtitle: "Thông tin thời sự nóng nhất trong ngày",
    imageurl: require("./images/sport.jpg"),
  },
];

const Item = ({ item }) => (
  <TouchableOpacity onPress={() => Alert.alert("AHIHI")}>
    <View style={styles.item}>
      <View>
        <Image style={styles.tinyLogo} source={item.imageurl} />
      </View>
      <View>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.subtitle}>{item.subtitle}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

const App = () => {
  const renderItem = ({ item }) => <Item item={item} />;

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    flexDirection: "row",
    backgroundColor: "#f1f1f1",
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  subtitle: {
    fontSize: 15,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
});

export default App;
